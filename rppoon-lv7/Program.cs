﻿using System;

namespace rppoon_lv7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 1:
            double[] sequenceToBePassedToTheConstructor = { 1, 0,
                    Math.Sqrt(2), Math.PI,
                    Math.E, (Math.Sqrt(5)+1)/2
                };
            NumberSequence numberSequence = new NumberSequence(sequenceToBePassedToTheConstructor);
            numberSequence.SetSortStrategy(new BubbleSort());
            numberSequence.Sort();
            Console.WriteLine("The sorted sequence is:");
            Console.WriteLine(numberSequence);
            //Task 2:
            numberSequence.SetSearchStrategy(new LinearSearch());
            Console.WriteLine("22/7 "+(numberSequence.SearchFor(22.0 / 7)?"is":"isn't")+" present there.");
            Console.WriteLine("311/99 " + (numberSequence.SearchFor(311.0 / 99) ? "is" : "isn't") + " present there.");
            //Task 3
            /*
             * Unfortunately, System.Diagnostics.PerformanceCounter seems to do nothing but crash the program if used on Linux.
             * Here is what it outputs:
             *            
             * The type name 'PerformanceCounter' could not be found in the namespace 'System.Diagnostics'.
             * This type has been forwarded to assembly
             * 'System.Diagnostics.PerformanceCounter, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
             * Consider adding a reference to that assembly.
            */
            /*
            SystemDataProvider provider = new SystemDataProvider();
            provider.Attach(new ConsoleLogger());
            while (true)
            {
                provider.GetCPULoad();
                provider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
            */
            //Task 4: See "SystemDataProvider.cs"
            //Task 5:
            DVD windows = new DVD("Windows 7", DVDType.SOFTWARE, 100);
            DVD movie = new DVD("Moon", DVDType.MOVIE, 50);
            Book book = new Book("Bible", 5);
            VHS oldMovie = new VHS("Wizard Of Oz",25);
            Console.WriteLine(windows);
            Console.WriteLine("Price with taxes: "+windows.Accept(new BuyVisitor()));
            //Task 6:
            Console.WriteLine("Price for renting: " + windows.Accept(new RentVisitor()));
            //Task 7:
            Cart cart = new Cart();
            cart.AddItem(windows);
            cart.AddItem(movie);
            cart.AddItem(book);
            cart.AddItem(oldMovie);
            Console.WriteLine(cart);
            Console.WriteLine("Price with taxes: " + cart.Accept(new BuyVisitor()));
        }
    }
}
