﻿using System;
namespace rppoon_lv7
{
    public class LinearSearch : SearchStrategy
    {
        public override int SearchFor(double x, double[] array)
        {
            for (int i = 0; i < array.Length; i++)
                if (Math.Abs(x - array[i]) < 1.0 / 1000)
                    return i;
            return -1;
        }
    }
}
