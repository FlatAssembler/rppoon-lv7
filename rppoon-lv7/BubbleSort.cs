﻿using System;
namespace rppoon_lv7
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int minimalIndex = i;
                for (int j = i + 1; j < array.Length; j++)
                    if (array[minimalIndex] > array[j])
                        minimalIndex = j;
                Swap(ref array[i], ref array[minimalIndex]);
            }
        }
    }
}
