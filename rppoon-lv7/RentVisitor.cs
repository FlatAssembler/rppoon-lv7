﻿using System;
namespace rppoon_lv7
{
    public class RentVisitor : IVisitor
    {
        private const double DVDTax = 0.18;
        private const double VHSTax = 0.10;
        private const double bookTax = 0.15;
        double IVisitor.Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return double.NaN;
            }
            return DVDItem.Price * (1 + DVDTax);
        }
        double IVisitor.Visit(VHS VHSItem)
        {
            return VHSItem.Price * (1 + DVDTax);
        }
        double IVisitor.Visit(Book book)
        {
            return book.Price * (1 + bookTax);
        }
    }
}
