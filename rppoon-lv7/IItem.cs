﻿using System;
namespace rppoon_lv7
{
    public interface IItem
    {
        double Accept(IVisitor visitor);
        double Price { get; }
    }
}
