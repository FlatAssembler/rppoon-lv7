﻿using System;
namespace rppoon_lv7
{
    public class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine(DateTime.Now + "-> CPU load: " +
                provider.CPULoad + " Available RAM: " + provider.AvailableRAM);
        }
    }
}
