﻿using System;
using System.Collections.Generic;

namespace rppoon_lv7
{
    public class Cart :IItem
    {
        private double price;
        public double Price { get { return price; } }
        private List<IItem> items;
        public Cart()
        {
            items = new List<IItem>();
        }
        public void AddItem(IItem item)
        {
            items.Add(item);
            price += item.Price;
        }
        public void RemoveItem(IItem item)
        {
            if (items.Remove(item))
                price -= item.Price;
        }
        public override string ToString()
        {
            string representation = "{\n";
            foreach (var item in items)
                representation += item+"\n";
            representation += "Total price: " + price + "\n}";
            return representation;
        }
        public double Accept(IVisitor visitor)
        {
            double priceWithTaxes = 0;
            foreach (var item in items)
            {
                priceWithTaxes += item.Accept(visitor);
            }
            return priceWithTaxes;
        }
    }
}
