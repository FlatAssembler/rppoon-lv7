﻿using System;
namespace rppoon_lv7
{
    public abstract class SearchStrategy
    {
        public abstract int SearchFor(double x, double[] array);
    }
}
